// Завдання 6
// Дан об'єкт employee. Додайте в нього властивості age і salary, не змінюючи
// початковий об'єкт (повинен бути створений новий об'єкт, який буде включати всі необхідні властивості). Виведіть новостворений об'єкт в консоль

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const propertyNewEmployee = { ...employee, age: 55, salary: "$3500" };
console.log(propertyNewEmployee);

