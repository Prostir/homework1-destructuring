// Завдання 3
// У нас є об'єкт user:
// const user1 = {
//   name: "John",
//   years: 30
// };

// Напишіть деструктурірующее присвоювання, яке:

// властивість name присвоїть в змінну name
// властивість years присвоїть в змінну age
// властивість isAdmin присвоїть в змінну isAdmin (false, якщо немає такої властивості)

// Виведіть змінні на екран.


const user1 = {
      name: "John",
      years: 30
    };


const { name, years: age, isAdmin = false } = user1;
console.log(name, age, isAdmin);
