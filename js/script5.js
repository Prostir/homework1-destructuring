// Завдання 5
// Дан масив книг. Вам потрібно додати в нього ще одну книгу, 
// не змінюючи існуючий масив (в результаті операції повинен бути створений новий масив).

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

const bookNewAdd = [...books, bookToAdd];
console.log(bookNewAdd);

